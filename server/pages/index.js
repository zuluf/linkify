/**
 * Application routes
 * bind crude actions for each page controller
 */
const Home = require('./home');
const NotFound = require('./notFound');

module.exports = (server) => {

	/**
	 * Init home controller
	 */
	const home = new Home(server);
	const notFound = new NotFound(server);
}