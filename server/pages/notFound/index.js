/**
 * Application home controller
 */

module.exports = class NotFound {
	constructor (server) {
		this.template = 'notFound';

		server.use('*', this.render.bind(this));
	}

	render (req, res, next) {
		res.render(this.template, {
			model: {}
		});
		res.end();
	}
}