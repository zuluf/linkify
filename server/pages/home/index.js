/**
 * Application home controller
 */
const passport = require('passport');
const User = require('../../models/user');

module.exports = class Home {
	constructor (server) {
		this.template = 'home';

		server.get('/', this.render.bind(this));
	}

	render (req, res, next) {
		User.findOne({where : {
			id: 1
		}}).then((user) => {
			user = user || req.session.user;

			res.render(this.template, {
				model: {
					user: user
				}
			});

			res.end();
		})

		// debugger;

	}
}