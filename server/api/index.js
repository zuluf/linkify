/**
 * Application routes
 * bind crude actions for each page controller
 */
const User = require('./user');
const Link = require('./link');
const Scrape = require('./scrape');
const Tag = require('./tag');

module.exports = (server) => {

	server.use('/api/', (req, res, next) => {
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Content-Type", "application/json");

		return next();
	});

	/**
	 * Init home controller
	 */
	const user = new User(server);
	const link = new Link(server);
	const tag = new Tag(server);
	const scrape = new Scrape(server);
}