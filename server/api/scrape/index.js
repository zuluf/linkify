/**
 * Application home controller
 */
const passport = require('passport');
const Scraper = require('../../scripts/scraper');

module.exports = class User {
	constructor (server) {
		server.post('/api/scrape/url', this.scrape.bind(this));
	}

	scrape (req, res, next) {
		const { userUrl } = req.body;

		Scraper(this.validate(userUrl)).then((data) => {
			res.send({
				scraped: data
			});
			res.end();

		}).catch((err) => {
			res.send({
				error: err
			});
			res.end();
		})
	}

	/**
	 * Task 2. Validate user url
	 * @param  {String} url
	 * @return {String}
	 */
	validate (url) {
		return url;
	}
}