/**
 * Application home controller
 */

module.exports = class Tag {
	constructor (server) {
		server.get('/api/tags', this.render.bind(this));
	}

	render (req, res, next) {
		res.send(JSON.stringify({
			links: []
		}));
		res.end();
	}
}