/**
 * Application home controller
 */
const passport = require('passport');

module.exports = class User {
	constructor (server) {
		server.post('/api/user/login', this.login.bind(this));
		server.post('/api/user/logout', this.logout.bind(this));
		server.get('/api/user', this.render.bind(this));
	}

	login (req, res, next) {
		passport.authenticate('local', (err, user, info) => {
			req.session.user = user;
			req.session.cookie.maxAge = Date.now() + (86400 * 1000)
			res.send({
				user: user
			});
			res.end();
		})(req, res, next);
	}

	logout (req, res, next) {
		req.session.user = null;
		res.send({
			user: null
		});
		res.end();
	}

	render (req, res, next) {
		res.send({
			user: {}
		});
		res.end();
	}
}