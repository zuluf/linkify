/**
 * Application home controller
 */
const Link = require('../../models/link');

module.exports = class Link {
	constructor (server) {
		server.post('/api/links/add', this.addLink.bind(this));
		server.get('/api/links', this.render.bind(this));
	}

	addLink (req, res, next) {
		const { user } = req.session;
		const { userUrl } = req.body;

		if (!user) {
			res.send({
				error: 'Please login to continue'
			});
			res.end();
		}

		const newLink = new Link({
			user_id: user.id,
			url: userUrl
		});

		debugger

		newLink.then((link) => {
			res.send({
				link: link
			});
			res.end();
		}).catch((err) => {
			res.send({
				error: err
			});
			res.end();
		});
	}

	render (req, res, next) {
		res.send(JSON.stringify({
			links: []
		}));
		res.end();
	}
}