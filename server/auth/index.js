/**
 * Application auth settings using npm passport
 */
const passport = require('passport');
const { Strategy } = require('passport-local');

module.exports = (server) => {
	const defaults = {
		id: 1,
		first_name: 'john',
		last_name: 'doe',
		email: 'john@doe.com',
		username: 'johndoe',
		password: '123'
	}

	passport.use(new Strategy({
		session: true
	}, (username, password, next) => {
		return next(null, defaults);
	}));

	passport.serializeUser(function(user, next) {
		next(null, defaults.user_id);
	});

	passport.deserializeUser(function(id, next) {
		next(null, defaults);
	});
}