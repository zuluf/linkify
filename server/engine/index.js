/**
 * React + Redux server renderer function
 *
 * Runction renders required pre-built page template
 * Wraps the application in a Redux.Provider element
 * and initiates the Provider.props.store object
 *
 * Engine was purposely built without es2015 syntax,
 * so we could avoid wrapping express app instance inside of a babel preset
 */

'use strict';

const React = require('react');
const ReactDOM = require('react-dom/server');
const ReactRedux = require('react-redux');
const Redux = require('redux');
const view = require('./view');
const appRoot = require('../../build/js/pages.bundle');


/**
 * ReduxEngine function
 *
 * @param  {string}   pageName page template name to render
 * @param  {object}   pageData Redux.Provider store raw data, passed by the Expresso controller
 * @param  {Function} next     callback function
 * @return {void}
 */
const Engine = function ReduxEngine (pageName, pageData, next) {
	/**
	 * Application Layout compiled component
	 * @type {Function}
	 */
	const Layout = appRoot && appRoot.layout;

	if (typeof Layout !== 'function') {
		return next(new Error('ReduxEngine - Layout component is missing in ./app/dist/js/pages.js'), null);
	}

	/**
	 * Application NotFound compiled component
	 * @type {Function}
	 */
	const NotFound = appRoot && appRoot.notFound;

	if (typeof NotFound !== 'function') {
		return next(new Error('ReduxEngine - NotFound component is missing in ./app/dist/js/pages.js'), null);
	}

	/**
	 * Current page compiled component. Defaults to NotFound component
	 * @type {Function}
	 */
	const pageComponent = appRoot[pageName] || NotFound;

	/**
	 * Define ReactRedux.Provider component
	 * @type {Function}
	 */
	const Provider = ReactRedux.Provider;

	/**
	 * Extend model, and set the defaults
	 * @type {Object}
	 * @todo : Add ReduxEngine config options for overriding or filtering pageData object properties
	 */
	const { model } = pageData;

	/**
	 * Create Redux.Provider store instance
	 * @type {Object}
	 */
	const store = appRoot.appStore.storeInstance((model || {}));

	/**
	 * Create current page component React.Element
	 * @type {Object}
	 */
	const pageElement = React.createElement(pageComponent);

	/**
	 * Create Redux.Provider component React.Element
	 * @type {Object}
	 */
	const providerElement = React.createElement(Provider, {
		store: store
	}, pageElement);

	/**
	 * Render application provider wrapper before rewinding page helmet meta
	 */
	const renderedProvider = ReactDOM.renderToString(providerElement);

	/**
	 * Rewind current helmet meta data; Defaults to empty object
	 */
	const meta = (appRoot.helmet && appRoot.helmet.rewind()) || {};

	/**
	 * Create Layout component React.Element and pass the Provider rendered html
	 * @type {Object}
	 * @todo : Add ReduxEngine config options for custom global data holder
	 */
	let layoutElement = React.createElement(Layout, {
		html: renderedProvider,
		store: store,
		meta: meta,
		data: 'window.STORE = ' + JSON.stringify(store.getState())
	});

	/**
	 * Render the current layout to HTML static markup
	 * We only need the Redux.Provider rendered and handled by react at this point
	 * @type {String}
	 */
	let layoutHTML = ReactDOM.renderToString(layoutElement);

	/**
	 * Pass the compiled HTML
	 */
	next(null, layoutHTML);
};

/**
 * Export express view override functions
 */
Engine.view = view;

module.exports = Engine;
