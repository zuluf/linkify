/**
 * Application server bootstrap
 */
'use strict';

const Express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const Session = require('express-session');

const Engine = require('./engine');
const Auth = require('./auth');

/**
 * Create express server
 * @type {Object}
 */

const auth = Auth();
const server = new Express();

/**
 * Set application static folder
 */
server.use(Express.static('build'));

/**
 * Set application render Engine
 */
server.engine('jsx', Engine);

/**
 * Set jsx as the view engine
 */
server.set('view engine', 'jsx');

/**
 * Set react custom View function
 */
server.set('view', Engine.view);

/**
 * Attach request services
 */
server.use(bodyParser.urlencoded({
	extended: true
}));
server.use(cookieParser());
server.use(compression());

server.use(Session({
	secret: 'a2V5Ym9hcmQgY2F0',
	resave: true,
	saveUninitialized: true,
	cookie: {
		maxAge: (86400 * 1000)// set to one day, @todo: add to app config
	}
}));


server.use(passport.initialize());
server.use(passport.session());


module.exports = server;