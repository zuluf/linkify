/**
 * url scraper
 */
const request = require('request');
const striptags = require('striptags');

const parseResponse = (responseBody) => {
	let error = null;

	if (typeof responseBody !== "string") {
		error = 'Parse error';
	}

	if (!error) {
		const pureContent = striptags(responseBody);
		/**
		 * @todo: do the scraping
		 */
	}

	return {
		error: error,
		data: {}
	};
}

const Scraper = (url) => {

	return new Promise((resolve, reject) => {
		request(url, (error, response, body) => {
			const parsed = parseResponse(body);

			if (error || parsed.error) {
				reject(error || parsed.error);
			} else {
				resolve();
			}
		});
	});
}

module.exports = Scraper;