/**
 * Database connection
 *
 * @todo: add application database config
 */
const Sequelize = require('sequelize');
const path = require('path');

const sequelize = new Sequelize('pgdb', 'root', '', {
	host: 'localhost',
	dialect: 'sqlite',
	logging: false,
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},

	storage: path.join(__dirname, '../../../data/db.sqlite')
});

module.exports = sequelize;