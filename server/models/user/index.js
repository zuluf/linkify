/**
 * User model
 */
const Sequelize = require('sequelize');
const db = require('../database');
const bcrypt = require("bcrypt-nodejs");

const User = db.define('app_user', {
	first_name: {
		type: Sequelize.STRING
	},
	last_name: {
		type: Sequelize.STRING
	},
	email: {
		type: Sequelize.STRING
	},
	password: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true,
	paranoid: true,
	createdAt: 'createdAt',
	updatedAt: 'updatedAt',
	deletedAt: 'deletedAt',

	freezeTableName: true,

	instanceMethods: {
		generateHash: (password) => {
			return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
		},
		validPassword: (password) => {
			return bcrypt.compareSync(password, this.local.password);
		}
	}
});

User.sync({force: true});

module.exports = User;