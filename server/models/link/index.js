/**
 * Item model
 */
const Sequelize = require('sequelize');
const db = require('../database');

const Link = db.define('app_links', {
	user_id: {
		type: Sequelize.INTEGER
	},
	url: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true,
	paranoid: true,
	createdAt: 'createdAt',
	updatedAt: 'updatedAt',
	deletedAt: 'deletedAt',

	freezeTableName: true
});

Link.sync({force: true});

module.exports = Link;