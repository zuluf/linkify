/**
 * Connect to the database and bind models
 */

const db = require('./database');
const path = require('path');

// const User = require('./user');
// const Link = require('./link');
module.exports = () => {
	db.import(path.join(__dirname, '/user'))
	db.import(path.join(__dirname, '/link'))
}