/**
 * Application webpack config
 */
'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssImport = require('postcss-import');
const postcssCustomProperties = require('postcss-custom-properties');
const postcssNested = require('postcss-nested');
const postcssCustomMedia = require('postcss-custom-media');
const postcssPxtorem = require('postcss-pxtorem');

module.exports = [{
	name: 'client',
	entry: path.join(__dirname, '/client/main.js'),
	output: {
		path: path.join(__dirname, '/build/js'),
		filename: 'app.min.js'
	},
	postcss: function (webpack) {
		return [
			postcssImport({
				addDependencyTo: webpack,
				path: ['./client/styles/', './client/']
			}),
			postcssCustomProperties(),
			postcssNested(),
			postcssCustomMedia(),
			postcssPxtorem(),
			autoprefixer()
		];
	},

	plugins: [
		new ExtractTextPlugin('../css/style.css', { allChunks: true })
	],

	devtool: 'inline-source-map',

	module: {
		loaders: [{
			test: /\.css$/,
			loader: ExtractTextPlugin.extract('style-loader',
				'css-loader?modules&importLoaders=1&localIdentName=[hash:base64:5]!postcss-loader')
		},
		{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: 'babel-loader',
			query: {
				presets: ['react', 'es2015']
			}
		}, {
			test: /\.json$/,
			loader: 'json-loader'
		}, {
			test: /\.md$/,
			loader: 'null-loader'
		}, {
			test: /\.(png|jpg)?$/,
			loader: 'url',
			query: {
				limit: 25000,
				name: './build/img/[name].[ext]'
			}
		}, {
			test: /\.svg$/,
			loader: 'file-loader',
			query: {
				limit: 25000,
				name: './build/img/[name].[ext]'
			}
		}, {
			test: /\.woff$/,
			loader: 'url'
		}]
	},

	resolve: {
		extensions: ['', '.js', '.jsx', '.json']
	}
}, {
	name: 'serverBuild',
	entry: path.join(__dirname, '/client/index.js'),
	output: {
		path: path.join(__dirname, './build/js'),
		filename: 'pages.bundle.js',
		libraryTarget: 'commonjs2'
	},
	postcss: function (webpack) {
		return [
			postcssImport({
				addDependencyTo: webpack,
				path: ['./client/styles/', './client/']
			}),
			postcssCustomProperties(),
			postcssNested(),
			postcssCustomMedia(),
			postcssPxtorem(),
			autoprefixer()
		];
	},

	plugins: [
		new ExtractTextPlugin('../css/style.css', { allChunks: true })
	],

	module: {
		loaders: [{
			test: /\.css$/,
			loader: ExtractTextPlugin.extract('style-loader',
				'css-loader?modules&importLoaders=1&localIdentName=[hash:base64:5]!postcss-loader')
		},
		{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: 'babel-loader',
			query: {
				presets: ['react', 'es2015']
			}
		}, {
			test: /\.json$/,
			loader: 'json-loader'
		}, {
			test: /\.md$/,
			loader: 'null-loader'
		}, {
			test: /\.(png|jpg)?$/,
			loader: 'url',
			query: {
				limit: 25000,
				name: './build/img/[name].[ext]'
			}
		}, {
			test: /\.svg$/,
			loader: 'file-loader',
			query: {
				limit: 25000,
				name: './build/img/[name].[ext]'
			}
		}, {
			test: /\.woff$/,
			loader: 'url'
		}]
	},

	resolve: {
		extensions: ['', '.js', '.jsx', '.json']
	}
}];