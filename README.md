Linkify
========

### Installation
Application requires you have preinstalled nodejs version 7.x and npm 3.x

```
cd /path/to/linkify/
npm install
```

### Run the app
cd into to the application directory and run

```
node index.js
```

Application will create the database tables, start the server, and you can check it out on
[localhost](http://127.0.0.1:9090)


### Client side libraries
Application is using React + Redux + Page router for the single page application state, rendering and routing.

```
https://www.npmjs.com/package/react
https://www.npmjs.com/package/react-redux
https://www.npmjs.com/package/redux
https://www.npmjs.com/package/page
```

Mapping of the api restfull data to the SPA redux store is implemented using the redux-saga approach and an ajax client request wrapper

```
https://www.npmjs.com/package/redux-saga
https://www.npmjs.com/package/reqwest
```

The redux-saga package are mainly used over redux-thunk, since it's making it easier to implement unit testing for the application data flow per each application api endpoint.