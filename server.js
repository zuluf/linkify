/**
 * Application server bootstrap
 */
const server = require('./server/index.js');
const vhost = require('vhost');
const config = require('./config');
const Api = require('./server/api');
const Pages = require('./server/pages');

if (typeof config !== "object") {
	throw Error('Appliction config is missing or invalid');
}

const serverConfig = config.server || {};
const serverPort = serverConfig.port || 9090;
let serverHost = serverConfig.host || '127.0.0.1';

if (serverConfig.vhosts) {
	serverConfig.vhosts.map((appVhost) => {
		server.use(vhost(appVhost, function (req, res, next) {
			next();
		}));
	})
}


/**
 * Init application api
 */
Api(server);

/**
 * Init application routes and pages
 */
Pages(server);

/**
 * Start the app
 */
server.listen(serverPort, serverHost, () => {
    console.log(`✔ Server started successfully. Go to http://${serverHost}:${serverPort}`);
});
