/**
 * Application redux bootstarp
 */
import { createStore, applyMiddleware, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';

/**
 * Load application sagas
 */
import userSagas from './user/sagas';
import linkSagas from './links/sagas';
import scrapeSagas from './scrape/sagas';

/**
 * Load application reducers
 */
import link from './links/reducer';
import user from './user/reducer';
import scrape from './scrape/reducer';

const sagaMiddleware = createSagaMiddleware();

const storeInstance = (initialState) => {

	const appReducers = combineReducers({
		user: user,
		link: link,
		scrape: scrape
	});

	/**
	 * Leave room for a list of combineReducers and applyMiddleware of sagas or thunks or both
	 */
	return createStore(appReducers, (initialState || {}), applyMiddleware(sagaMiddleware));
}

const startSagas = () => {
	sagaMiddleware.run(userSagas);
	sagaMiddleware.run(linkSagas);
	sagaMiddleware.run(scrapeSagas);
}

export default {
	storeInstance: storeInstance,
	startSagas: startSagas
}