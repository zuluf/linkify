/**
 * User model saga calls
 */

import { takeLatest, call, put } from 'redux-saga/effects';
import Actions from './actions';
import reqwest from '../../helpers/request';

const userLoggedIn = (response) => {
	const { user } = response

	return {
		type: Actions.ActionTypes.USER_LOGGEDIN,
		payload: user
	}
}

const userLoggedOut = (response) => {
	return {
		type: Actions.ActionTypes.USER_LOGGEDOUT,
		payload: null
	}
}

const userLogin = function *userLogin (action) {
	const { type, payload: { url, data } } = action

	try {
		const response = yield call(reqwest.post, url, data);
		yield put(userLoggedIn(response));
	} catch (e) {
		put(userLoggedOut());
	}
}

const userLogout = function *userLogout (action) {
	const { type, payload: { url, data } } = action

	try {
		const response = yield call(reqwest.post, url, data);
		yield put(userLoggedOut());
	} catch (e) {
		// userLoggedOut()
	} finally {
		put(userLoggedOut());
	}
}

export default function *userSagas () {
	yield takeLatest(Actions.ActionTypes.USER_LOGIN, userLogin);
	yield takeLatest(Actions.ActionTypes.USER_LOGOUT, userLogout);
}