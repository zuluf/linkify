/**
 * Define user redux actons
 */

const ActionTypes = {
	USER_LOGIN: 'USER_LOGIN',
	USER_LOGGEDIN: 'USER_LOGGEDIN',
	USER_LOGGEDOUT: 'USER_LOGGEDOUT',
	USER_LOGOUT: 'USER_LOGOUT'
}

const login = (params) => {
	const url = '/api/user/login';
	const { username, password } = params;

	return {
		type: ActionTypes.USER_LOGIN,
		payload: {
			url: url,
			data: {
				username: username,
				password: password
			}
		}
	};
}

const logout = (params) => {
	const url = '/api/user/logout';

	return {
		type: ActionTypes.USER_LOGOUT,
		payload: {
			url: url
		}
	};
}

export default {
	ActionTypes,
	login,
	logout
}