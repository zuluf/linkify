/**
 * User store reducer
 */
import Actions from './actions';

export default function user (state = {}, action) {
	const { payload } = action;

	/**
	 * Decouple current state
	 */
	const reduced = Object.assign({}, state);

	switch (action.type) {
	case Actions.ActionTypes.USER_LOGGEDIN:
		return Object.assign(reduced, payload);
	case Actions.ActionTypes.USER_LOGGEDOUT:
		return {}
	default:
		return reduced;
	}
}