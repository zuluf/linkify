/**
 * User model saga calls
 */
import { takeLatest, call, put } from 'redux-saga/effects';
import Actions from './actions';
import reqwest from '../../helpers/request';

const linkAdded = (response) => {
	const { link } = response

	return {
		type: Actions.ActionTypes.LINK_ADDED,
		payload: link
	}
}

const addUserLink = function *addUserLink (action) {
	const { type, payload: { url, data } } = action;

	try {
		const response = yield call(reqwest.post, url, data);
		yield put(linkAdded(response));
	} catch (e) {
		console.log(e);
	}
}

export default function *userSagas () {
	yield takeLatest(Actions.ActionTypes.LINK_ADD, addUserLink);
}