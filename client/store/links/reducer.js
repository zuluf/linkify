/**
 * User store reducer
 */
import Actions from './actions';

export default function user (state = {}, action) {
	const { payload } = action;

	/**
	 * Decouple current state
	 */
	const reduced = Object.assign({}, state);

	switch (action.type) {
	case Actions.ActionTypes.LINK_ADDED:
		const links = reduced.links || [];

		links.push(payload);

		return Object.assign(reduced, { links: links });
	default:
		return reduced;
	}
}