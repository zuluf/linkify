/**
 * Define user redux actons
 */

const ActionTypes = {
	LINK_ADD: 'LINK_ADD',
	LINK_ADDED: 'LINK_ADDED',
}

const addLink = (data) => {
	const apiUrl = '/api/links/add';

	return {
		type: ActionTypes.LINK_ADD,
		payload: {
			url: apiUrl,
			data: data
		}
	};
}

export default {
	ActionTypes,
	addLink
}