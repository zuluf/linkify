/**
 * User model saga calls
 */

import { takeLatest, call, put } from 'redux-saga/effects';
import Actions from './actions';
import reqwest from '../../helpers/request';

const linkScraped = (response) => {
	const { scraped } = response

	return {
		type: Actions.ActionTypes.SCRAPE_URL_DONE,
		payload: scraped
	}
}

const scrapeUserLink = function *addUserLink (action) {
	const { type, payload: { url, data } } = action;

	try {
		const response = yield call(reqwest.post, url, data);
		yield put(linkScraped(response));
	} catch (e) {
		console.log(e);
	}
}

export default function *userSagas () {
	yield takeLatest(Actions.ActionTypes.SCRAPE_URL, scrapeUserLink);
}