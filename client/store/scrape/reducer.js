/**
 * User store reducer
 */
import Actions from './actions';

export default function user (state = {}, action) {
	const { payload } = action;

	/**
	 * Decouple current state
	 */
	const reduced = Object.assign({}, state);

	switch (action.type) {
	case Actions.ActionTypes.SCRAPE_URL_DONE:
		return Object.assign(reduced, { scraped: payload });
	default:
		return reduced;
	}
}