/**
 * Define user redux actons
 */

const ActionTypes = {
	SCRAPE_URL: 'SCRAPE_URL',
	SCRAPE_URL_DONE: 'SCRAPE_URL_DONE',
}

const scrapeLink = (data) => {
	const apiUrl = '/api/scrape/url';

	return {
		type: ActionTypes.SCRAPE_URL,
		payload: {
			url: apiUrl,
			data: data
		}
	};
}

export default {
	ActionTypes,
	scrapeLink
}