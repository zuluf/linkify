/**
 * Application request helper
 */
import reqwest from 'reqwest'

const get = (url) => {
	return reqwest({
		url: url,
		type: 'json',
		method: 'get'
	});
}

const post = (url, data) => {
	return reqwest({
		url: url,
		type: 'json',
		method: 'post',
		data: data,
		contentType: 'application/x-www-form-urlencoded'
	});
}

const put = (url, data) => {
	return reqwest({
		url: url,
		type: 'json',
		method: 'put',
		data: data
	});
}

const destroy = (url, data) => {
	return reqwest({
		url: url,
		type: 'json',
		method: 'delete',
		data: data
	});
}

export default {
	get: get,
	post: post,
	put: put,
	destroy: destroy
}