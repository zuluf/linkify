/**
 * Login form component
 *
 * @type {React.Component}
 */
import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UserActions from '../../store/user/actions';
import styles from './style.css';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			password: ''
		}

		this.onInputChange = this.onInputChange.bind(this)
	}

	login () {
		this.props.actions.login(Object.assign({}, this.state));
	}

	onInputChange (event) {
		const state = {};
		const { target, target: { name, value } } = event;

		/**
		 * @todo: add validation
		 */
		state[name] = value;

		this.setState(state);
	}

	render () {
		return (
		<div id='login-form' className={styles.container}>
			<h3>Please login to continue</h3>

			<ul>
				<li>
					<label>Username
						<input
							type="text"
							name="username"
							onChange={this.onInputChange}
						/>
					</label>
				</li>
				<li>
					<label>Password
						<input
							type="text"
							name="password"
							onChange={this.onInputChange}
						/>
					</label>
				</li>
				<li>
					<a
						href="#"
						onClick={(event) => {
							event.preventDefault();
							this.login()
						}}
					> login </a>
				</li>
			</ul>
		</div>
	)}
}

/**
 * Bind redux.store user data to component props
 *
 * @param  {Object} Redux.store
 * @return {Object}
 */
const stateToProps = (state) => {
	return Object.assign({}, state.user);
}

/**
 * Bind User model redux actions to component properties
 *
 * @param  {Function} dispatch
 * @return {Object}
 */
const dispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators(UserActions, dispatch)
	}
}

/**
 * Connect to Redux.store and export Login component
 */
export default connect(stateToProps, dispatchToProps)(Login);