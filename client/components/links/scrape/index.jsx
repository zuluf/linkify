/**
 * Scrape user link component
 *
 * @type {React.Component}
 */
import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ScrapeActions from '../../../store/scrape/actions';

class ScrapeLinks extends Component {
	constructor(props) {
		super(props);

		this.state = {
			userUrl: ''
		}

		this.onInputChange = this.onInputChange.bind(this)
	}

	scrapeLink () {
		this.props.actions.scrapeLink(Object.assign({}, this.state));
	}

	onInputChange (event) {
		const state = {};
		const { target, target: { name, value } } = event;

		/**
		 * @todo: add validation
		 */
		state[name] = value;

		this.setState(state);
	}

	render () {
		return (<div>
				<label>Scrape the link for keywords
					<input
						type="text"
						name="userUrl"
						onChange={this.onInputChange}
					/>
				</label>
				<a
						href="#"
						onClick={(event) => {
							event.preventDefault();
							this.scrapeLink()
						}}
					> add link </a>
			</div>
		);
	}
}

/**
 * Bind Scrape model redux actions to component properties
 *
 * @param  {Function} dispatch
 * @return {Object}
 */
const dispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators(ScrapeActions, dispatch)
	}
}

/**
 * Connect to Redux.store and export ScrapeLinks component
 */
export default connect(null, dispatchToProps)(ScrapeLinks);