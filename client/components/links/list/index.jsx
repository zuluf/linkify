/**
 * User saved links component
 *
 * @type {React.Component}
 */
import React, { PropTypes } from 'react';

const LinksList = (props) => (
	<div>
		{props.links.length > 0 ?
			<h3>Your saved links</h3>
			:
			''
		}
	</div>
)

/**
 * Set component default/required properties
 * @type {Object}
 */
LinksList.propTypes = {
	links: PropTypes.array.isRequired
};

export default LinksList;