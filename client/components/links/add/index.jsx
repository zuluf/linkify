/**
 * Add new link form component
 *
 * @type {React.Component}
 */
import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LinkActions from '../../../store/links/actions';

class AddLinks extends Component {
	constructor(props) {
		super(props);

		this.state = {
			userUrl: ''
		}

		this.onInputChange = this.onInputChange.bind(this)
	}

	addLink () {
		this.props.actions.addLink(Object.assign({}, this.state));
	}

	onInputChange (event) {
		const state = {};
		const { target, target: { name, value } } = event;

		/**
		 * @todo: add validation
		 */
		state[name] = value;

		this.setState(state);
	}

	render () {
		return (<div>
				<label>Add a new linkify url
					<input
						type="text"
						name="userUrl"
						onChange={this.onInputChange}
					/>
				</label>
				<a
						href="#"
						onClick={(event) => {
							event.preventDefault();
							this.addLink()
						}}
					> add link </a>
			</div>
		);
	}
}


/**
 * Bind Link model redux actions to component properties
 *
 * @param  {Function} dispatch
 * @return {Object}
 */
const dispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators(LinkActions, dispatch)
	}
}

/**
 * Connect to Redux.store and export AddLinks component
 */
export default connect(null, dispatchToProps)(AddLinks);