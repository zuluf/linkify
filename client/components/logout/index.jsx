/**
 * Logout link app component
 *
 * @type {React.Component}
 */
import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UserActions from '../../store/user/actions';

class Logout extends Component {
	constructor(props) {
		super(props);
	}

	logout () {
		this.props.actions.logout();
	}

	render () {
		return (
		<div id='logout'>
			<a
				href="#"
				onClick={(event) => {
					event.preventDefault();
					this.logout()
				}}
			> logout </a>
		</div>
	)}
}

/**
 * Bind User model redux actions to component properties
 *
 * @param  {Function} dispatch
 * @return {Object}
 */
const dispatchToProps = (dispatch) => {
	return {
		actions: bindActionCreators(UserActions, dispatch)
	}
}

/**
 * Connect to Redux.store and export Logout component
 */
export default connect(null, dispatchToProps)(Logout);