/**
 * notFound React template
 */
import React from 'react';
import Helmet from 'react-helmet';

export default (props) => (
	<div>
		<Helmet
			title="app not found"
		/>
		<div id='notFound'>
			<h3>404 route not found</h3>
			<a href='/'>Home</a>
		</div>
	</div>
)