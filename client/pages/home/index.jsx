/**
 * Application home page
 */
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import Login from '../../components/login';
import Logout from '../../components/logout';
import Links from '../../components/links/list';
import Scrape from '../../components/links/scrape';
import AddLink from '../../components/links/add';
import Helmet from 'react-helmet';
import styles from './style.css';

class Home extends Component {
	constructor(props) {
		super(props);
	}

	render () {
		const { user, links } = this.props;

		return (
			<div>
				<Helmet
					title="app home page"
				/>
				{!user || !user.id ? <Login /> :
					<div>
						<h3>Welcome {user.first_name}</h3> <Logout />
						<Scrape />
						<Links
							links={links || []}
						/>
					</div>
				}
			</div>
		)
	}
};

/**
 * Bind application state to the page component props
 *
 * @param  {Object} dispatch
 * @return {Object}
 */
const stateToProps = (state) => {
	return Object.assign({}, state);
}

export default connect(stateToProps)(Home);