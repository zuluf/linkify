/**
 * Application Layout React Component
 */
import React from 'react';

export default (props) => (
	<html lang="en-US">
		<head>
			<meta charSet='utf-8' />
			<link rel='stylesheet' href='css/style.css' />

			{/**
			  * Render application meta data and page title
			  */}
			{props.meta.title.toComponent()}
			{props.meta.meta.toComponent()}

			<script type="text/javascript" dangerouslySetInnerHTML={{ __html: props.data }}></script>
		</head>
		<body>
			<div id="carndriver" dangerouslySetInnerHTML={{ __html: props.html }}></div>
			<script type="text/javascript" src="js/app.min.js"></script>
		</body>
	</html>
);