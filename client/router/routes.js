/**
 * Single Page Application context routes
 *
 * @todo : add unified config from Expresso app.config.routes
 */
import Page from 'page';
import Render from './render';
import appRoot from '../';

export default function Routes (store) {
    /**
     * Bind homepage route
     */
    Page('/', Render(appRoot['home'], store));

    /**
     * Bind notFound page route
     */
    Page('*', Render(appRoot['notFound'], store));

    /**
     * Initiate router
     */
    Page.start();
}
