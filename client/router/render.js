/**
 * Renders
 * @param {[type]} store [description]
 */
import { render } from 'react-dom';
import { createElement } from 'react';
import { Provider } from 'react-redux';

export default function Render (pageElement, store) {
	/**
	 * DOM application root element
	 */
	const appRoot = document.getElementById('carndriver');

	window.state = store;
	/**
	 * Action component renderer function
	 * @param {Object}   Context  router path context
	 * @param {Function} next     nextEnter
	 */
	return function Component () {
		/**
		 * Create provider element
		 * @type {Redux.Provider}
		 */
		let providerElement = createElement(Provider, {
			store: store,
			children: createElement(pageElement)
		});

		/**
		 * Render application
		 */
		render(providerElement, appRoot);
	};
}
