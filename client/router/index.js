/**
 * Application router instance
 */

import appStore from '../store';
import Routes from './routes';

const Router = (viewModel) => {
    /**
     * Bind routes and boot up the app
     */
    new Routes(appStore.storeInstance(viewModel));

    /**
     * Bind model sagas
     */
    appStore.startSagas();
}

/**
 * Find viewmodel from the global VAST object, or default to notFound
 * @type {Object}
 */
const viewModel = Object.assign({}, (window.STORE || {}));

Router(viewModel);