/**
 * Export client side modules
 */
import appStore from './store';
import layout from './layout';
import notFound from './pages/notFound';
import home from './pages/home';
import helmet from 'react-helmet';

module.exports = {
	layout: layout,
	notFound: notFound,
	home: home,
	helmet: helmet,
	appStore: appStore
};
